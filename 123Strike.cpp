﻿#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>
#include <tchar.h>
#include "pch.h"
#include "MSXStruc.h"
#include <vector>

#define DLL_EXPORT extern "C" __declspec(dllexport)

int NumberOfArguments = 8;
DLL_EXPORT BOOL __stdcall MSXInfo(MSXDLLDef* a_psDLLDef)
{
	std::string szMayCopyright = "Copyright (c) TradingIndicators.com, 2021";
	strncpy(a_psDLLDef->szCopyright, (szMayCopyright.c_str()), (szMayCopyright.size()));

	a_psDLLDef->iNFuncs = 4;
	
	// This is the number of functions our DLL exports for access in MetaStock.  Here we have 4: 
	// (1) Cloud type, (2) Trading signals, (3) Long IET, (4) Short IET

	a_psDLLDef->iVersion = MSX_VERSION; 
	return MSX_SUCCESS;
}

DLL_EXPORT BOOL __stdcall MSXNthFunction(int a_iNthFunc, MSXFuncDef* a_psFuncDef)
{
	BOOL l_bRtrn = MSX_SUCCESS;
	switch (a_iNthFunc)
	{
	case 0:
		strcpy(a_psFuncDef->szFunctionName, "CurrentTrend");
		strcpy(a_psFuncDef->szFunctionDescription, "Returns values representing the current trend. 1 = Up trend; -1 = Down trend |");
		a_psFuncDef->iNArguments = NumberOfArguments; // число аргументов
		break;
	case 1:
		strcpy(a_psFuncDef->szFunctionName, "TradingSignals");
		strcpy(a_psFuncDef->szFunctionDescription, "Returns values representing different trading signals. |");
		a_psFuncDef->iNArguments = NumberOfArguments; // число аргументов
		break;
	case 2:
		strcpy(a_psFuncDef->szFunctionName, "LongIET");
		strcpy(a_psFuncDef->szFunctionDescription, "Returns the IET long efficiency percentage.");
		a_psFuncDef->iNArguments = NumberOfArguments; // число аргументов
		break;
	case 3:
		strcpy(a_psFuncDef->szFunctionName, "ShortIET");
		strcpy(a_psFuncDef->szFunctionDescription, "Returns the IET short efficiency percentage.");
		a_psFuncDef->iNArguments = NumberOfArguments; // число аргументов
		break;
	default:
		l_bRtrn = MSX_ERROR;
		break;
	}
	return l_bRtrn;
}


DLL_EXPORT BOOL __stdcall MSXNthArg(int a_iNthFunc, int a_iNthArg,
	MSXFuncArgDef* a_psFuncArgDef)
{
	BOOL l_bRtrn = MSX_SUCCESS;


	switch (a_iNthArg)
	{
	case 0:
		a_psFuncArgDef->iArgType = MSXNumeric;    		// тип
	//	strcpy(a_psFuncArgDef->szArgName, szNAfan1Arg1); // имя
		break;
	case 1:
		a_psFuncArgDef->iArgType = MSXNumeric; 			// тип
		//strcpy(a_psFuncArgDef->szArgName, szNAfan1Arg2); // имя
		break;
	case 2:
		a_psFuncArgDef->iArgType = MSXNumeric; 			// тип
		//strcpy(a_psFuncArgDef->szArgName, szNAfan1Arg2); // имя
		break;
	case 3:
		a_psFuncArgDef->iArgType = MSXNumeric; 			// тип
		//strcpy(a_psFuncArgDef->szArgName, szNAfan1Arg2); // имя
		break;
	case 4:
		a_psFuncArgDef->iArgType = MSXNumeric; 			// тип
		//strcpy(a_psFuncArgDef->szArgName, szNAfan1Arg2); // имя
		break;
	case 5:
		a_psFuncArgDef->iArgType = MSXNumeric; 			// тип
		//strcpy(a_psFuncArgDef->szArgName, szNAfan1Arg2); // имя
		break;
	case 6:
		a_psFuncArgDef->iArgType = MSXNumeric; 			// тип
		//strcpy(a_psFuncArgDef->szArgName, szNAfan1Arg2); // имя
		
	break;
	case 7:
		a_psFuncArgDef->iArgType = MSXNumeric; 			// тип
		//strcpy(a_psFuncArgDef->szArgName, szNAfan1Arg2); // имя
		break;
	default:
		l_bRtrn = MSX_ERROR;
		break;
	}


	return l_bRtrn;
}
int startOffset = 1;

float vectorAverage(std::vector<float>& floats) {
	float avg = 0;
	for (int i = 0; i < floats.size(); i++) {
		avg += floats.at(i);
	}
	avg /= (floats.size() > 0 ? floats.size() : 1);

	return avg;
}

float vectorHighest(std::vector<float>& floats) {
	float highest = -340282346638528859811704183484516925440.0;
	for (int i = 0; i < floats.size(); i++) {
		if (floats.at(i) > highest) highest = floats.at(i);
	}
	return highest;
}



// The function we're exporting in MSXNthFunction
DLL_EXPORT BOOL __stdcall CurrentTrend(const MSXDataRec* dataRec,
	const MSXDataInfoRecArgsArray* a_psArrayArgs,
	const MSXNumericArgsArray* a_psNumericArgs,
	const MSXStringArgsArray* a_psStringArgs,
	const MSXCustomArgsArray* a_psCustomArgs,
	MSXResultRec* a_psResult)
{
	BOOL returnValue = MSX_SUCCESS;


	// Input arguments. These will be read from the argument array properly later on.
	int VolumePeriod = (int)a_psNumericArgs->fNumerics[0];
	int VolatilityPeriod = (int)a_psNumericArgs->fNumerics[1];
	bool ExitOnHighVolumeReversal = (int)a_psNumericArgs->fNumerics[2] == 1;
	int LongPartialExits = (int)a_psNumericArgs->fNumerics[3];
	int ShortPartialExits = (int)a_psNumericArgs->fNumerics[4];
	int FastMAPeriod = (int)a_psNumericArgs->fNumerics[5];
	int SlowMAPeriod = (int)a_psNumericArgs->fNumerics[6];
	int tStyle = (int)a_psNumericArgs->fNumerics[7];
	bool canTakeLongs = tStyle == 0 ? true : tStyle == 1 ? true : false;
	bool canTakeShorts = tStyle == 0 ? true : tStyle == 1 ? false : true;




	int seriesSize = dataRec->sClose.iLastValid;

	float* atrSeries;
	bool* bearishCloudSeries;
	bool* bullishCloudSeries;
	bool* currentlyLongSeries;
	bool* currentlyShortSeries;
	// Allocate memory for the value series'
	atrSeries = new float[seriesSize + 1];
	bearishCloudSeries = new bool[seriesSize + 1];
	bullishCloudSeries = new bool[seriesSize + 1];
	currentlyLongSeries = new bool[seriesSize + 1];
	currentlyShortSeries = new bool[seriesSize + 1];

	// Initialize all of the series' values to NaN
	for (int i = 0; i <= seriesSize; i++) {
		atrSeries[i] = NAN;
		bearishCloudSeries[i] = false;
		bullishCloudSeries[i] = false;
		currentlyLongSeries[i] = false;
		currentlyShortSeries[i] = false;
	}









	// Start index offset to make sure we have valid data values. Eg. we need to start at beginning + 1 to make sure we can get the close of the previous bar.

	// Iterate over the data series

	std::vector<float> trueRangesVector;
	std::vector<float> fastMAClosesVector;
	std::vector<float> slowMAClosesVector;
	std::vector<float> volumesVector;







	// IET Variables
	float lep = NAN, sep = NAN;
	bool currentlyLong = false, currentlyShort = false;
	int spec = 0, lpec = 0;
	int totalLongTrades = 0, winningLongTrades = 0;
	int totalShortTrades = 0, winningShortTrades = 0;
	int lastShortEntryIndex = -1, lastLongEntryIndex = -1;



	for (int i = dataRec->sClose.iFirstValid + startOffset; i <= dataRec->sClose.iLastValid; i++) {

		// Initialize the value return to MS to 0.
		a_psResult->psResultArray->pfValue[i] = 0;

		// The signal (logic) that will be returned to MetaStock for this index.
		float signal = 0;
		if (lpec == LongPartialExits) lpec = 0;
		if (spec == ShortPartialExits) spec = 0;
		bool lpeTaken = false, speTaken = false, lfeTaken = false, sfeTaken = false, leTaken = false, seTaken = false,
			sreTaken = false, lreTaken = false;


		currentlyLongSeries[i] = currentlyLongSeries[i - 1];
		currentlyShortSeries[i] = currentlyShortSeries[i - 1];

		float open = dataRec->sOpen.pfValue[i];
		float high = dataRec->sHigh.pfValue[i];
		float low = dataRec->sLow.pfValue[i];
		float close = dataRec->sClose.pfValue[i];
		float volume = dataRec->sVol.pfValue[i];

		float open_1 = dataRec->sOpen.pfValue[i - 1];
		float high_1 = dataRec->sHigh.pfValue[i - 1];
		float low_1 = dataRec->sLow.pfValue[i - 1];
		float close_1 = dataRec->sClose.pfValue[i - 1];
		fastMAClosesVector.push_back(close);
		if (fastMAClosesVector.size() > FastMAPeriod)
			fastMAClosesVector.erase(fastMAClosesVector.begin());
		slowMAClosesVector.push_back(close);
		if (slowMAClosesVector.size() > SlowMAPeriod)
			slowMAClosesVector.erase(slowMAClosesVector.begin());
		volumesVector.push_back(volume);
		if (volumesVector.size() > VolumePeriod)
			volumesVector.erase(volumesVector.begin());
		
		bool highestVolume = volume == vectorHighest(volumesVector);

		float volumeAverage = vectorAverage(volumesVector);
		bool aboveAverageVolume = volume > volumeAverage;

		float trueRange = max(max(abs(high - low), abs(high - close_1)), abs(close_1 - low));
		trueRangesVector.push_back(trueRange);
		if (trueRangesVector.size() > VolatilityPeriod)
			trueRangesVector.erase(trueRangesVector.begin());

		float ATR = vectorAverage(trueRangesVector);
		float fastMA = vectorAverage(fastMAClosesVector);
		float slowMA = vectorAverage(slowMAClosesVector);
		atrSeries[i] = ATR;

		// Continue if there isn't enough data for the ATR 'VolatilityPeriod' bars ago to have been calculated
		if (i < VolatilityPeriod + startOffset)
			continue;


		float ATR_VolatilityPeriod = atrSeries[i - VolatilityPeriod];
		float close_VolatilityPeriod = dataRec->sClose.pfValue[i - VolatilityPeriod];

		bool ATR_VolatilityPeriodIsNaN = ATR_VolatilityPeriod != ATR_VolatilityPeriod;
		bool close_VolatilityPeriodisNaN = close_VolatilityPeriod != close_VolatilityPeriod;

		// NAN Checks
		if (
			ATR_VolatilityPeriodIsNaN ||
			close_VolatilityPeriodisNaN ||
			ATR_VolatilityPeriod == NAN ||
			close_VolatilityPeriod == NAN
			)
			continue;
		float floatZero = 0.0f;
		float float1Hundred = 100.0F;

		float indicatorLinearSlope = (ATR - ATR_VolatilityPeriod) / VolatilityPeriod;
		float priceLinearSlope = fastMA >= slowMA ? 1.0 : -1.0;
		float changeInPrice = abs(close - close_VolatilityPeriod);
		float atrMultiplied = 1.50 * ATR;

		bool indicatorLinearSlopeIsNaN = indicatorLinearSlope != indicatorLinearSlope;
		bool priceLinearSlopeIsNaN = priceLinearSlope != priceLinearSlope;
		bool changeInPriceIsNaN = changeInPrice != changeInPrice;
		bool atrMultipliedIsNaN = atrMultiplied != atrMultiplied;
		if (
			indicatorLinearSlopeIsNaN ||
			priceLinearSlopeIsNaN ||
			changeInPriceIsNaN ||
			atrMultipliedIsNaN
			)
			continue;

		bearishCloudSeries[i] = indicatorLinearSlope >= floatZero && priceLinearSlope < floatZero && changeInPrice >= atrMultiplied;
		bullishCloudSeries[i] = indicatorLinearSlope >= floatZero&& priceLinearSlope >= floatZero && changeInPrice >= atrMultiplied;

		if (!aboveAverageVolume) {
			bearishCloudSeries[i] = false;
			bullishCloudSeries[i] = false;
		}

		if (currentlyLongSeries[i] && ((highestVolume && close > open)) && ExitOnHighVolumeReversal && priceLinearSlope < 0) {
			bearishCloudSeries[i] = true;
			bullishCloudSeries[i] = false;
		}
		if (bullishCloudSeries[i] && ((close - open > 2 * 1.5 * ATR))) {
			bullishCloudSeries[i] = false;
		}

		if (currentlyShortSeries[i] && ((highestVolume && close < open)) && ExitOnHighVolumeReversal && priceLinearSlope > 0) {
			bearishCloudSeries[i] = true;
			bullishCloudSeries[i] = false;
		}
		if (bearishCloudSeries[i] && ((open - close > 2 * 1.5 * ATR))) {
			bearishCloudSeries[i] = false;
		}





		// If we are in a bearish cloud
		if (bearishCloudSeries[i]) {
			// And we are long with partial exits remaining
			if (currentlyLongSeries[i]) {
				if (canTakeLongs && lpec < LongPartialExits) {
					// Take a final long partial (not reversal) exit.
					lpec++;

					lpeTaken = true;
					lfeTaken = true;
					lreTaken = true;
					currentlyLongSeries[i] = false;

					float realized = close - lep;
					if (realized >= floatZero) winningLongTrades++;
					totalLongTrades++;

				}

				currentlyLongSeries[i] = false;
			}

			if (canTakeShorts) {

				// Go short if not already
				if (/*spec == 0 && */ !currentlyShortSeries[i]) {
					currentlyShortSeries[i] = true;
					lastShortEntryIndex = i;
					seTaken = true;
					sep = close;
					spec = 0;
				}
			}
		}


		// If we are short and just snapped out of a bearish cloud, take a partial exit
		if (currentlyShortSeries[i] && !bearishCloudSeries[i] && bearishCloudSeries[i - 1] && i - lastShortEntryIndex > 0.618*FastMAPeriod) {
			speTaken = true;
			spec++;
			// If we have reached the max partial exits, mark it as a final partial exit
			if (spec == ShortPartialExits) {
				sfeTaken = true;
				// And mark the short position as closed
				currentlyShortSeries[i] = false;
			}
			float realized = sep - close;
			if (realized >= floatZero) winningShortTrades++;
			totalShortTrades++;
		}



		// If we are in a bullish cloud
		if (bullishCloudSeries[i]) {
			// And we are short with partial exits remaining
			if (currentlyShortSeries[i]) {
				if (canTakeShorts && spec < ShortPartialExits) {
					// Take a final short partial (not reversal) exit.
					speTaken = true;
					sfeTaken = true;
					sreTaken = true;
					float realized = sep - close;
					if (realized >= floatZero) winningShortTrades++;
					spec++;
					totalShortTrades++;
				}
				currentlyShortSeries[i] = false;
			}

			if (canTakeLongs) {

				// Go long if not already
				if (!currentlyLongSeries[i]) {
					currentlyLongSeries[i] = true;
					lastLongEntryIndex = i;
					leTaken = true;
					lep = close;
					lpec = 0;
				}
			}
		}

		// If we are long and just snapped out of a bullish cloud, take a partial exit
		if (currentlyLongSeries[i] && !bullishCloudSeries[i] && bullishCloudSeries[i - 1] && i - lastLongEntryIndex > 0.618 * FastMAPeriod) {
			lpeTaken = true;
			lpec++;
			// If we have reached the max partial exits, mark it as a final partial exit
			if (lpec == LongPartialExits) {
				lfeTaken = true;
				// And mark the short position as closed
				currentlyLongSeries[i] = false;
			}
			float realized = close - lep;
			if (realized >= floatZero) winningLongTrades++;
			totalLongTrades++;
		}


		signal = priceLinearSlope;
		a_psResult->psResultArray->pfValue[i] = signal;
	}






	// Basic error handling
	if (a_psResult->psResultArray->iFirstValid < 0 || a_psResult->psResultArray->iLastValid < 0
		|| a_psResult->psResultArray->iLastValid < a_psResult->psResultArray->iFirstValid ||
		a_psResult->psResultArray->iFirstValid < dataRec->sClose.iFirstValid ||
		a_psResult->psResultArray->iLastValid > dataRec->sClose.iLastValid)
		returnValue = MSX_ERROR;


	delete[] atrSeries;
	delete[] bearishCloudSeries;
	delete[] bullishCloudSeries;
	delete[] currentlyLongSeries;
	delete[] currentlyShortSeries;

	return returnValue;
}



// The function we're exporting in MSXNthFunction
DLL_EXPORT BOOL __stdcall TradingSignals(const MSXDataRec* dataRec,
	const MSXDataInfoRecArgsArray* a_psArrayArgs,
	const MSXNumericArgsArray* a_psNumericArgs,
	const MSXStringArgsArray* a_psStringArgs,
	const MSXCustomArgsArray* a_psCustomArgs,
	MSXResultRec* a_psResult)
{
		BOOL returnValue = MSX_SUCCESS;


		// Input arguments. These will be read from the argument array properly later on.
		int VolumePeriod = (int)a_psNumericArgs->fNumerics[0];
		int VolatilityPeriod = (int)a_psNumericArgs->fNumerics[1];
		bool ExitOnHighVolumeReversal = (int)a_psNumericArgs->fNumerics[2] == 1;
		int LongPartialExits = (int)a_psNumericArgs->fNumerics[3];
		int ShortPartialExits = (int)a_psNumericArgs->fNumerics[4];
		int FastMAPeriod = (int)a_psNumericArgs->fNumerics[5];
		int SlowMAPeriod = (int)a_psNumericArgs->fNumerics[6];
		int tStyle = (int)a_psNumericArgs->fNumerics[7];
		bool canTakeLongs = tStyle == 0 ? true : tStyle == 1 ? true : false;
		bool canTakeShorts = tStyle == 0 ? true : tStyle == 1 ? false : true;




		int seriesSize = dataRec->sClose.iLastValid;
		float* atrSeries;
		bool* bearishCloudSeries;
		bool* bullishCloudSeries;
		bool* currentlyLongSeries;
		bool* currentlyShortSeries;
		// Allocate memory for the value series'
		atrSeries = new float[seriesSize + 1];
		bearishCloudSeries = new bool[seriesSize + 1];
		bullishCloudSeries = new bool[seriesSize + 1];
		currentlyLongSeries = new bool[seriesSize + 1];
		currentlyShortSeries = new bool[seriesSize + 1];

		// Initialize all of the series' values to NaN
		for (int i = 0; i <= seriesSize; i++) {
			atrSeries[i] = NAN;
			bearishCloudSeries[i] = false;
			bullishCloudSeries[i] = false;
			currentlyLongSeries[i] = false;
			currentlyShortSeries[i] = false;
		}











		// Start index offset to make sure we have valid data values. Eg. we need to start at beginning + 1 to make sure we can get the close of the previous bar.

		// Iterate over the data series

		std::vector<float> trueRangesVector;
		std::vector<float> fastMAClosesVector;
		std::vector<float> slowMAClosesVector;
		std::vector<float> volumesVector;







		// IET Variables
		float lep = NAN, sep = NAN;
		bool currentlyLong = false, currentlyShort = false;
		int spec = 0, lpec = 0;
		int totalLongTrades = 0, winningLongTrades = 0;
		int totalShortTrades = 0, winningShortTrades = 0;
		int lastShortEntryIndex = -1, lastLongEntryIndex = -1;



		for (int i = dataRec->sClose.iFirstValid + startOffset; i <= dataRec->sClose.iLastValid; i++) {

			// Initialize the value return to MS to 0.
			a_psResult->psResultArray->pfValue[i] = -1.0;

			// The signal (logic) that will be returned to MetaStock for this index.
			float signal = 0;
			if (lpec == LongPartialExits) lpec = 0;
			if (spec == ShortPartialExits) spec = 0;
			bool lpeTaken = false, speTaken = false, lfeTaken = false, sfeTaken = false, leTaken = false, seTaken = false,
				sreTaken = false, lreTaken = false;


			currentlyLongSeries[i] = currentlyLongSeries[i - 1];
			currentlyShortSeries[i] = currentlyShortSeries[i - 1];

			float open = dataRec->sOpen.pfValue[i];
			float high = dataRec->sHigh.pfValue[i];
			float low = dataRec->sLow.pfValue[i];
			float close = dataRec->sClose.pfValue[i];
			float volume = dataRec->sVol.pfValue[i];

			float open_1 = dataRec->sOpen.pfValue[i - 1];
			float high_1 = dataRec->sHigh.pfValue[i - 1];
			float low_1 = dataRec->sLow.pfValue[i - 1];
			float close_1 = dataRec->sClose.pfValue[i - 1];
			fastMAClosesVector.push_back(close);
			if (fastMAClosesVector.size() > FastMAPeriod)
				fastMAClosesVector.erase(fastMAClosesVector.begin());
			slowMAClosesVector.push_back(close);
			if (slowMAClosesVector.size() > SlowMAPeriod)
				slowMAClosesVector.erase(slowMAClosesVector.begin());
			volumesVector.push_back(volume);
			if (volumesVector.size() > VolumePeriod)
				volumesVector.erase(volumesVector.begin());

			bool highestVolume = volume == vectorHighest(volumesVector);

			float volumeAverage = vectorAverage(volumesVector);
			bool aboveAverageVolume = volume > volumeAverage;

			float trueRange = max(max(abs(high - low), abs(high - close_1)), abs(close_1 - low));
			trueRangesVector.push_back(trueRange);
			if (trueRangesVector.size() > VolatilityPeriod)
				trueRangesVector.erase(trueRangesVector.begin());

			float ATR = vectorAverage(trueRangesVector);
			float fastMA = vectorAverage(fastMAClosesVector);
			float slowMA = vectorAverage(slowMAClosesVector);
			atrSeries[i] = ATR;

			// Continue if there isn't enough data for the ATR 'VolatilityPeriod' bars ago to have been calculated
			if (i < VolatilityPeriod + startOffset)
				continue;


			float ATR_VolatilityPeriod = atrSeries[i - VolatilityPeriod];
			float close_VolatilityPeriod = dataRec->sClose.pfValue[i - VolatilityPeriod];

			bool ATR_VolatilityPeriodIsNaN = ATR_VolatilityPeriod != ATR_VolatilityPeriod;
			bool close_VolatilityPeriodisNaN = close_VolatilityPeriod != close_VolatilityPeriod;

			// NAN Checks
			if (
				ATR_VolatilityPeriodIsNaN ||
				close_VolatilityPeriodisNaN ||
				ATR_VolatilityPeriod == NAN ||
				close_VolatilityPeriod == NAN
				)
				continue;
			float floatZero = 0.0f;
			float float1Hundred = 100.0F;

			float indicatorLinearSlope = (ATR - ATR_VolatilityPeriod) / VolatilityPeriod;
			float priceLinearSlope = fastMA >= slowMA ? 1.0 : -1.0;
			float changeInPrice = abs(close - close_VolatilityPeriod);
			float atrMultiplied = 1.50 * ATR;

			bool indicatorLinearSlopeIsNaN = indicatorLinearSlope != indicatorLinearSlope;
			bool priceLinearSlopeIsNaN = priceLinearSlope != priceLinearSlope;
			bool changeInPriceIsNaN = changeInPrice != changeInPrice;
			bool atrMultipliedIsNaN = atrMultiplied != atrMultiplied;
			if (
				indicatorLinearSlopeIsNaN ||
				priceLinearSlopeIsNaN ||
				changeInPriceIsNaN ||
				atrMultipliedIsNaN
				)
				continue;

			bearishCloudSeries[i] = indicatorLinearSlope >= floatZero && priceLinearSlope < floatZero&& changeInPrice >= atrMultiplied;
			bullishCloudSeries[i] = indicatorLinearSlope >= floatZero && priceLinearSlope >= floatZero && changeInPrice >= atrMultiplied;

			if (!aboveAverageVolume) {
				bearishCloudSeries[i] = false;
				bullishCloudSeries[i] = false;
			}

			if (currentlyLongSeries[i] && ((highestVolume && close > open)) && ExitOnHighVolumeReversal && priceLinearSlope < 0) {
				bearishCloudSeries[i] = true;
				bullishCloudSeries[i] = false;
			}
			if (bullishCloudSeries[i] && ((close - open > 2 * 1.5 * ATR))) {
				bullishCloudSeries[i] = false;
			}

			if (currentlyShortSeries[i] && ((highestVolume && close < open)) && ExitOnHighVolumeReversal && priceLinearSlope > 0) {
				bearishCloudSeries[i] = true;
				bullishCloudSeries[i] = false;
			}
			if (bearishCloudSeries[i] && ((open - close > 2 * 1.5 * ATR))) {
				bearishCloudSeries[i] = false;
			}





			// If we are in a bearish cloud
			if (bearishCloudSeries[i]) {
				// And we are long with partial exits remaining
				if (currentlyLongSeries[i]) {
					if (canTakeLongs && lpec < LongPartialExits) {
						// Take a final long partial (not reversal) exit.
						lpec++;

						lpeTaken = true;
						lfeTaken = true;
						lreTaken = true;
						currentlyLongSeries[i] = false;

						float realized = close - lep;
						if (realized >= floatZero) winningLongTrades++;
						totalLongTrades++;

					}

					currentlyLongSeries[i] = false;
				}

				if (canTakeShorts) {

					// Go short if not already
					if (/*spec == 0 && */ !currentlyShortSeries[i]) {
						currentlyShortSeries[i] = true;
						lastShortEntryIndex = i;
						seTaken = true;
						sep = close;
						spec = 0;
					}
				}
			}



			// If we are short and just snapped out of a bearish cloud, take a partial exit
			if (currentlyShortSeries[i] && !bearishCloudSeries[i] && bearishCloudSeries[i - 1] && i - lastShortEntryIndex > 0.618 * FastMAPeriod) {
				speTaken = true;
				spec++;
				// If we have reached the max partial exits, mark it as a final partial exit
				if (spec == ShortPartialExits) {
					sfeTaken = true;
					// And mark the short position as closed
					currentlyShortSeries[i] = false;
				}
				float realized = sep - close;
				if (realized >= floatZero) winningShortTrades++;
				totalShortTrades++;
			}



			// If we are in a bullish cloud
			if (bullishCloudSeries[i]) {
				// And we are short with partial exits remaining
				if (currentlyShortSeries[i]) {
					if (canTakeShorts && spec < ShortPartialExits) {
						// Take a final short partial (not reversal) exit.
						speTaken = true;
						sfeTaken = true;
						sreTaken = true;
						float realized = sep - close;
						if (realized >= floatZero) winningShortTrades++;
						spec++;
						totalShortTrades++;
					}
					currentlyShortSeries[i] = false;
				}

				if (canTakeLongs) {

					// Go long if not already
					if (!currentlyLongSeries[i]) {
						currentlyLongSeries[i] = true;
						lastLongEntryIndex = i;
						leTaken = true;
						lep = close;
						lpec = 0;
					}
				}
			}

			// If we are long and just snapped out of a bullish cloud, take a partial exit
			if (currentlyLongSeries[i] && !bullishCloudSeries[i] && bullishCloudSeries[i - 1] && i - lastLongEntryIndex > 0.618 * FastMAPeriod) {
				lpeTaken = true;
				lpec++;
				// If we have reached the max partial exits, mark it as a final partial exit
				if (lpec == LongPartialExits) {
					lfeTaken = true;
					// And mark the short position as closed
					currentlyLongSeries[i] = false;
				}
				float realized = close - lep;
				if (realized >= floatZero) winningLongTrades++;
				totalLongTrades++;
			}
			signal = (leTaken && !sreTaken) ? 0.0 : (leTaken && sreTaken) ? 10.0 :
				(seTaken && !lreTaken) ? 1.0 : (seTaken && lreTaken) ? 11.0 :
				(lpeTaken && !lfeTaken) ? 2.0 : (lpeTaken && lfeTaken) ? 4.0 :
				(speTaken && !sfeTaken) ? 3.0 : (speTaken && sfeTaken) ? 5.0 :
				-1.0;

				float winningLongTrades_Float = winningLongTrades;
				float totalLongTrades_Float = totalLongTrades;
				float longEfficiency = (winningLongTrades_Float / (max(1, totalLongTrades_Float))) * 100.0F;
				
				float winningShortTrades_Float = winningShortTrades;
				float totalShortTrades_Float = totalShortTrades;
				float shortEfficiency = (winningShortTrades_Float / (max(1, totalShortTrades_Float))) * 100.0F;

				a_psResult->psResultArray->pfValue[i] = signal;
	}






	// Basic error handling
	if (a_psResult->psResultArray->iFirstValid < 0 || a_psResult->psResultArray->iLastValid < 0
		|| a_psResult->psResultArray->iLastValid < a_psResult->psResultArray->iFirstValid ||
		a_psResult->psResultArray->iFirstValid < dataRec->sClose.iFirstValid ||
		a_psResult->psResultArray->iLastValid > dataRec->sClose.iLastValid)
		returnValue = MSX_ERROR;


	delete[] atrSeries;
	delete[] bearishCloudSeries;
	delete[] bullishCloudSeries;
	delete[] currentlyLongSeries;
	delete[] currentlyShortSeries;

	return returnValue;
}


// The function we're exporting in MSXNthFunction
DLL_EXPORT BOOL __stdcall LongIET(const MSXDataRec* dataRec,
	const MSXDataInfoRecArgsArray* a_psArrayArgs,
	const MSXNumericArgsArray* a_psNumericArgs,
	const MSXStringArgsArray* a_psStringArgs,
	const MSXCustomArgsArray* a_psCustomArgs,
	MSXResultRec* a_psResult)
{
	BOOL returnValue = MSX_SUCCESS;


	// Input arguments. These will be read from the argument array properly later on.
	int VolumePeriod = (int)a_psNumericArgs->fNumerics[0];
	int VolatilityPeriod = (int)a_psNumericArgs->fNumerics[1];
	bool ExitOnHighVolumeReversal = (int)a_psNumericArgs->fNumerics[2] == 1;
	int LongPartialExits = (int)a_psNumericArgs->fNumerics[3];
	int ShortPartialExits = (int)a_psNumericArgs->fNumerics[4];
	int FastMAPeriod = (int)a_psNumericArgs->fNumerics[5];
	int SlowMAPeriod = (int)a_psNumericArgs->fNumerics[6];
	int tStyle = (int)a_psNumericArgs->fNumerics[7];
	bool canTakeLongs = tStyle == 0 ? true : tStyle == 1 ? true : false;
	bool canTakeShorts = tStyle == 0 ? true : tStyle == 1 ? false : true;




	int seriesSize = dataRec->sClose.iLastValid;
	float* atrSeries;
	bool* bearishCloudSeries;
	bool* bullishCloudSeries;
	bool* currentlyLongSeries;
	bool* currentlyShortSeries;
	// Allocate memory for the value series'
	atrSeries = new float[seriesSize + 1];
	bearishCloudSeries = new bool[seriesSize + 1];
	bullishCloudSeries = new bool[seriesSize + 1];
	currentlyLongSeries = new bool[seriesSize + 1];
	currentlyShortSeries = new bool[seriesSize + 1];

	// Initialize all of the series' values to NaN
	for (int i = 0; i <= seriesSize; i++) {
		atrSeries[i] = NAN;
		bearishCloudSeries[i] = false;
		bullishCloudSeries[i] = false;
		currentlyLongSeries[i] = false;
		currentlyShortSeries[i] = false;
	}







	// Start index offset to make sure we have valid data values. Eg. we need to start at beginning + 1 to make sure we can get the close of the previous bar.

	// Iterate over the data series

	std::vector<float> trueRangesVector;
	std::vector<float> fastMAClosesVector;
	std::vector<float> slowMAClosesVector;
	std::vector<float> volumesVector;


	// IET Variables
	float lep = NAN, sep = NAN;
	bool currentlyLong = false, currentlyShort = false;
	int spec = 0, lpec = 0;
	int totalLongTrades = 0, winningLongTrades = 0;
	int totalShortTrades = 0, winningShortTrades = 0;
	int lastShortEntryIndex = -1, lastLongEntryIndex = -1;



	for (int i = dataRec->sClose.iFirstValid + startOffset; i <= dataRec->sClose.iLastValid; i++) {

		// Initialize the value return to MS to 0.
		a_psResult->psResultArray->pfValue[i] = 0;

		// The signal (logic) that will be returned to MetaStock for this index.
		float signal = 0;
		if (lpec == LongPartialExits) lpec = 0;
		if (spec == ShortPartialExits) spec = 0;
		bool lpeTaken = false, speTaken = false, lfeTaken = false, sfeTaken = false, leTaken = false, seTaken = false,
			sreTaken = false, lreTaken = false;


		currentlyLongSeries[i] = currentlyLongSeries[i - 1];
		currentlyShortSeries[i] = currentlyShortSeries[i - 1];

		float open = dataRec->sOpen.pfValue[i];
		float high = dataRec->sHigh.pfValue[i];
		float low = dataRec->sLow.pfValue[i];
		float close = dataRec->sClose.pfValue[i];
		float volume = dataRec->sVol.pfValue[i];

		float open_1 = dataRec->sOpen.pfValue[i - 1];
		float high_1 = dataRec->sHigh.pfValue[i - 1];
		float low_1 = dataRec->sLow.pfValue[i - 1];
		float close_1 = dataRec->sClose.pfValue[i - 1];
		fastMAClosesVector.push_back(close);
		if (fastMAClosesVector.size() > FastMAPeriod)
			fastMAClosesVector.erase(fastMAClosesVector.begin());
		slowMAClosesVector.push_back(close);
		if (slowMAClosesVector.size() > SlowMAPeriod)
			slowMAClosesVector.erase(slowMAClosesVector.begin());
		volumesVector.push_back(volume);
		if (volumesVector.size() > VolumePeriod)
			volumesVector.erase(volumesVector.begin());

		bool highestVolume = volume == vectorHighest(volumesVector);

		float volumeAverage = vectorAverage(volumesVector);
		bool aboveAverageVolume = volume > volumeAverage;

		float trueRange = max(max(abs(high - low), abs(high - close_1)), abs(close_1 - low));
		trueRangesVector.push_back(trueRange);
		if (trueRangesVector.size() > VolatilityPeriod)
			trueRangesVector.erase(trueRangesVector.begin());

		float ATR = vectorAverage(trueRangesVector);
		float fastMA = vectorAverage(fastMAClosesVector);
		float slowMA = vectorAverage(slowMAClosesVector);
		atrSeries[i] = ATR;

		// Continue if there isn't enough data for the ATR 'VolatilityPeriod' bars ago to have been calculated
		if (i < VolatilityPeriod + startOffset)
			continue;


		float ATR_VolatilityPeriod = atrSeries[i - VolatilityPeriod];
		float close_VolatilityPeriod = dataRec->sClose.pfValue[i - VolatilityPeriod];

		bool ATR_VolatilityPeriodIsNaN = ATR_VolatilityPeriod != ATR_VolatilityPeriod;
		bool close_VolatilityPeriodisNaN = close_VolatilityPeriod != close_VolatilityPeriod;

		// NAN Checks
		if (
			ATR_VolatilityPeriodIsNaN ||
			close_VolatilityPeriodisNaN ||
			ATR_VolatilityPeriod == NAN ||
			close_VolatilityPeriod == NAN
			)
			continue;
		float floatZero = 0.0f;
		float float1Hundred = 100.0F;

		float indicatorLinearSlope = (ATR - ATR_VolatilityPeriod) / VolatilityPeriod;
		float priceLinearSlope = fastMA >= slowMA ? 1.0 : -1.0;
		float changeInPrice = abs(close - close_VolatilityPeriod);
		float atrMultiplied = 1.50 * ATR;

		bool indicatorLinearSlopeIsNaN = indicatorLinearSlope != indicatorLinearSlope;
		bool priceLinearSlopeIsNaN = priceLinearSlope != priceLinearSlope;
		bool changeInPriceIsNaN = changeInPrice != changeInPrice;
		bool atrMultipliedIsNaN = atrMultiplied != atrMultiplied;
		if (
			indicatorLinearSlopeIsNaN ||
			priceLinearSlopeIsNaN ||
			changeInPriceIsNaN ||
			atrMultipliedIsNaN
			)
			continue;

		bearishCloudSeries[i] = indicatorLinearSlope >= floatZero && priceLinearSlope < floatZero&& changeInPrice >= atrMultiplied;
		bullishCloudSeries[i] = indicatorLinearSlope >= floatZero && priceLinearSlope >= floatZero && changeInPrice >= atrMultiplied;

		if (!aboveAverageVolume) {
			bearishCloudSeries[i] = false;
			bullishCloudSeries[i] = false;
		}

		if (currentlyLongSeries[i] && ((highestVolume && close > open)) && ExitOnHighVolumeReversal && priceLinearSlope < 0) {
			bearishCloudSeries[i] = true;
			bullishCloudSeries[i] = false;
		}
		if (bullishCloudSeries[i] && ((close - open > 2 * 1.5 * ATR))) {
			bullishCloudSeries[i] = false;
		}

		if (currentlyShortSeries[i] && ((highestVolume && close < open)) && ExitOnHighVolumeReversal && priceLinearSlope > 0) {
			bearishCloudSeries[i] = true;
			bullishCloudSeries[i] = false;
		}
		if (bearishCloudSeries[i] && ((open - close > 2 * 1.5 * ATR))) {
			bearishCloudSeries[i] = false;
		}





		// If we are in a bearish cloud
		if (bearishCloudSeries[i]) {
			// And we are long with partial exits remaining
			if (currentlyLongSeries[i]) {
				if (canTakeLongs && lpec < LongPartialExits) {
					// Take a final long partial (not reversal) exit.
					lpec++;

					lpeTaken = true;
					lfeTaken = true;
					lreTaken = true;
					currentlyLongSeries[i] = false;

					float realized = close - lep;
					if (realized >= floatZero) winningLongTrades++;
					totalLongTrades++;

				}

				currentlyLongSeries[i] = false;
			}

			if (canTakeShorts) {

				// Go short if not already
				if (/*spec == 0 && */ !currentlyShortSeries[i]) {
					currentlyShortSeries[i] = true;
					lastShortEntryIndex = i;
					seTaken = true;
					sep = close;
					spec = 0;
				}
			}
		}


		// If we are short and just snapped out of a bearish cloud, take a partial exit
		if (currentlyShortSeries[i] && !bearishCloudSeries[i] && bearishCloudSeries[i - 1] && i - lastShortEntryIndex > 0.618 * FastMAPeriod) {
			speTaken = true;
			spec++;
			// If we have reached the max partial exits, mark it as a final partial exit
			if (spec == ShortPartialExits) {
				sfeTaken = true;
				// And mark the short position as closed
				currentlyShortSeries[i] = false;
			}
			float realized = sep - close;
			if (realized >= floatZero) winningShortTrades++;
			totalShortTrades++;
		}



		// If we are in a bullish cloud
		if (bullishCloudSeries[i]) {
			// And we are short with partial exits remaining
			if (currentlyShortSeries[i]) {
				if (canTakeShorts && spec < ShortPartialExits) {
					// Take a final short partial (not reversal) exit.
					speTaken = true;
					sfeTaken = true;
					sreTaken = true;
					float realized = sep - close;
					if (realized >= floatZero) winningShortTrades++;
					spec++;
					totalShortTrades++;
				}
				currentlyShortSeries[i] = false;
			}

			if (canTakeLongs) {

				// Go long if not already
				if (!currentlyLongSeries[i]) {
					currentlyLongSeries[i] = true;
					lastLongEntryIndex = i;
					leTaken = true;
					lep = close;
					lpec = 0;
				}
			}
		}

		// If we are long and just snapped out of a bullish cloud, take a partial exit
		if (currentlyLongSeries[i] && !bullishCloudSeries[i] && bullishCloudSeries[i - 1] && i - lastLongEntryIndex > 0.618 * FastMAPeriod) {
			lpeTaken = true;
			lpec++;
			// If we have reached the max partial exits, mark it as a final partial exit
			if (lpec == LongPartialExits) {
				lfeTaken = true;
				// And mark the short position as closed
				currentlyLongSeries[i] = false;
			}
			float realized = close - lep;
			if (realized >= floatZero) winningLongTrades++;
			totalLongTrades++;
		}

		signal = (leTaken && !sreTaken) ? 0.5 : (leTaken && sreTaken) ? 10.0 :
			(seTaken && !lreTaken) ? 1.0 : (seTaken && lreTaken) ? 13.0 :
			(lpeTaken && !lfeTaken) ? 2.0 : (lpeTaken && lfeTaken) ? 4.0 :
			(speTaken && !sfeTaken) ? 3.0 : (speTaken && sfeTaken) ? 5.0 :
			0.0;

		float winningLongTrades_Float = winningLongTrades;
		float totalLongTrades_Float = totalLongTrades;
		float longEfficiency = (winningLongTrades_Float / (max(1, totalLongTrades_Float))) * 100.0F;

		float winningShortTrades_Float = winningShortTrades;
		float totalShortTrades_Float = totalShortTrades;
		float shortEfficiency = (winningShortTrades_Float / (max(1, totalShortTrades_Float))) * 100.0F;

		a_psResult->psResultArray->pfValue[i] = longEfficiency;
	}






	// Basic error handling
	if (a_psResult->psResultArray->iFirstValid < 0 || a_psResult->psResultArray->iLastValid < 0
		|| a_psResult->psResultArray->iLastValid < a_psResult->psResultArray->iFirstValid ||
		a_psResult->psResultArray->iFirstValid < dataRec->sClose.iFirstValid ||
		a_psResult->psResultArray->iLastValid > dataRec->sClose.iLastValid)
		returnValue = MSX_ERROR;

	delete[] atrSeries;
	delete[] bearishCloudSeries;
	delete[] bullishCloudSeries;
	delete[] currentlyLongSeries;
	delete[] currentlyShortSeries;


	return returnValue;
}


// The function we're exporting in MSXNthFunction
DLL_EXPORT BOOL __stdcall ShortIET(const MSXDataRec* dataRec,
	const MSXDataInfoRecArgsArray* a_psArrayArgs,
	const MSXNumericArgsArray* a_psNumericArgs,
	const MSXStringArgsArray* a_psStringArgs,
	const MSXCustomArgsArray* a_psCustomArgs,
	MSXResultRec* a_psResult)
{
	BOOL returnValue = MSX_SUCCESS;


	// Input arguments. These will be read from the argument array properly later on.
	int VolumePeriod = (int)a_psNumericArgs->fNumerics[0];
	int VolatilityPeriod = (int)a_psNumericArgs->fNumerics[1];
	bool ExitOnHighVolumeReversal = (int)a_psNumericArgs->fNumerics[2] == 1;
	int LongPartialExits = (int)a_psNumericArgs->fNumerics[3];
	int ShortPartialExits = (int)a_psNumericArgs->fNumerics[4];
	int FastMAPeriod = (int)a_psNumericArgs->fNumerics[5];
	int SlowMAPeriod = (int)a_psNumericArgs->fNumerics[6];
	int tStyle = (int)a_psNumericArgs->fNumerics[7];
	bool canTakeLongs = tStyle == 0 ? true : tStyle == 1 ? true : false;
	bool canTakeShorts = tStyle == 0 ? true : tStyle == 1 ? false : true;




	int seriesSize = dataRec->sClose.iLastValid;

	float* atrSeries;
	bool* bearishCloudSeries;
	bool* bullishCloudSeries;
	bool* currentlyLongSeries;
	bool* currentlyShortSeries;
	// Allocate memory for the value series'
	atrSeries = new float[seriesSize + 1];
	bearishCloudSeries = new bool[seriesSize + 1];
	bullishCloudSeries = new bool[seriesSize + 1];
	currentlyLongSeries = new bool[seriesSize + 1];
	currentlyShortSeries = new bool[seriesSize + 1];

	// Initialize all of the series' values to NaN
	for (int i = 0; i <= seriesSize; i++) {
		atrSeries[i] = NAN;
		bearishCloudSeries[i] = false;
		bullishCloudSeries[i] = false;
		currentlyLongSeries[i] = false;
		currentlyShortSeries[i] = false;
	}







	// Start index offset to make sure we have valid data values. Eg. we need to start at beginning + 1 to make sure we can get the close of the previous bar.

	// Iterate over the data series

	std::vector<float> trueRangesVector;
	std::vector<float> fastMAClosesVector;
	std::vector<float> slowMAClosesVector;
	std::vector<float> volumesVector;


	// IET Variables
	float lep = NAN, sep = NAN;
	bool currentlyLong = false, currentlyShort = false;
	int spec = 0, lpec = 0;
	int totalLongTrades = 0, winningLongTrades = 0;
	int totalShortTrades = 0, winningShortTrades = 0;
	int lastShortEntryIndex = -1, lastLongEntryIndex = -1;



	for (int i = dataRec->sClose.iFirstValid + startOffset; i <= dataRec->sClose.iLastValid; i++) {

		// Initialize the value return to MS to 0.
		a_psResult->psResultArray->pfValue[i] = 0;

		// The signal (logic) that will be returned to MetaStock for this index.
		float signal = 0;
		if (lpec == LongPartialExits) lpec = 0;
		if (spec == ShortPartialExits) spec = 0;
		bool lpeTaken = false, speTaken = false, lfeTaken = false, sfeTaken = false, leTaken = false, seTaken = false,
			sreTaken = false, lreTaken = false;


		currentlyLongSeries[i] = currentlyLongSeries[i - 1];
		currentlyShortSeries[i] = currentlyShortSeries[i - 1];

		float open = dataRec->sOpen.pfValue[i];
		float high = dataRec->sHigh.pfValue[i];
		float low = dataRec->sLow.pfValue[i];
		float close = dataRec->sClose.pfValue[i];
		float volume = dataRec->sVol.pfValue[i];

		float open_1 = dataRec->sOpen.pfValue[i - 1];
		float high_1 = dataRec->sHigh.pfValue[i - 1];
		float low_1 = dataRec->sLow.pfValue[i - 1];
		float close_1 = dataRec->sClose.pfValue[i - 1];
		fastMAClosesVector.push_back(close);
		if (fastMAClosesVector.size() > FastMAPeriod)
			fastMAClosesVector.erase(fastMAClosesVector.begin());
		slowMAClosesVector.push_back(close);
		if (slowMAClosesVector.size() > SlowMAPeriod)
			slowMAClosesVector.erase(slowMAClosesVector.begin());
		volumesVector.push_back(volume);
		if (volumesVector.size() > VolumePeriod)
			volumesVector.erase(volumesVector.begin());

		bool highestVolume = volume == vectorHighest(volumesVector);

		float volumeAverage = vectorAverage(volumesVector);
		bool aboveAverageVolume = volume > volumeAverage;

		float trueRange = max(max(abs(high - low), abs(high - close_1)), abs(close_1 - low));
		trueRangesVector.push_back(trueRange);
		if (trueRangesVector.size() > VolatilityPeriod)
			trueRangesVector.erase(trueRangesVector.begin());

		float ATR = vectorAverage(trueRangesVector);
		float fastMA = vectorAverage(fastMAClosesVector);
		float slowMA = vectorAverage(slowMAClosesVector);
		atrSeries[i] = ATR;

		// Continue if there isn't enough data for the ATR 'VolatilityPeriod' bars ago to have been calculated
		if (i < VolatilityPeriod + startOffset)
			continue;


		float ATR_VolatilityPeriod = atrSeries[i - VolatilityPeriod];
		float close_VolatilityPeriod = dataRec->sClose.pfValue[i - VolatilityPeriod];

		bool ATR_VolatilityPeriodIsNaN = ATR_VolatilityPeriod != ATR_VolatilityPeriod;
		bool close_VolatilityPeriodisNaN = close_VolatilityPeriod != close_VolatilityPeriod;

		// NAN Checks
		if (
			ATR_VolatilityPeriodIsNaN ||
			close_VolatilityPeriodisNaN ||
			ATR_VolatilityPeriod == NAN ||
			close_VolatilityPeriod == NAN
			)
			continue;
		float floatZero = 0.0f;
		float float1Hundred = 100.0F;

		float indicatorLinearSlope = (ATR - ATR_VolatilityPeriod) / VolatilityPeriod;
		float priceLinearSlope = fastMA >= slowMA ? 1.0 : -1.0;
		float changeInPrice = abs(close - close_VolatilityPeriod);
		float atrMultiplied = 1.50 * ATR;

		bool indicatorLinearSlopeIsNaN = indicatorLinearSlope != indicatorLinearSlope;
		bool priceLinearSlopeIsNaN = priceLinearSlope != priceLinearSlope;
		bool changeInPriceIsNaN = changeInPrice != changeInPrice;
		bool atrMultipliedIsNaN = atrMultiplied != atrMultiplied;
		if (
			indicatorLinearSlopeIsNaN ||
			priceLinearSlopeIsNaN ||
			changeInPriceIsNaN ||
			atrMultipliedIsNaN
			)
			continue;

		bearishCloudSeries[i] = indicatorLinearSlope >= floatZero && priceLinearSlope < floatZero&& changeInPrice >= atrMultiplied;
		bullishCloudSeries[i] = indicatorLinearSlope >= floatZero && priceLinearSlope >= floatZero && changeInPrice >= atrMultiplied;

		if (!aboveAverageVolume) {
			bearishCloudSeries[i] = false;
			bullishCloudSeries[i] = false;
		}

		if (currentlyLongSeries[i] && ((highestVolume && close > open)) && ExitOnHighVolumeReversal && priceLinearSlope < 0) {
			bearishCloudSeries[i] = true;
			bullishCloudSeries[i] = false;
		}
		if (bullishCloudSeries[i] && ((close - open > 2 * 1.5 * ATR))) {
			bullishCloudSeries[i] = false;
		}

		if (currentlyShortSeries[i] && ((highestVolume && close < open)) && ExitOnHighVolumeReversal && priceLinearSlope > 0) {
			bearishCloudSeries[i] = true;
			bullishCloudSeries[i] = false;
		}
		if (bearishCloudSeries[i] && ((open - close > 2 * 1.5 * ATR))) {
			bearishCloudSeries[i] = false;
		}





		// If we are in a bearish cloud
		if (bearishCloudSeries[i]) {
			// And we are long with partial exits remaining
			if (currentlyLongSeries[i]) {
				if (canTakeLongs && lpec < LongPartialExits) {
					// Take a final long partial (not reversal) exit.
					lpec++;

					lpeTaken = true;
					lfeTaken = true;
					lreTaken = true;
					currentlyLongSeries[i] = false;

					float realized = close - lep;
					if (realized >= floatZero) winningLongTrades++;
					totalLongTrades++;

				}

				currentlyLongSeries[i] = false;
			}

			if (canTakeShorts) {

				// Go short if not already
				if (/*spec == 0 && */ !currentlyShortSeries[i]) {
					currentlyShortSeries[i] = true;
					lastShortEntryIndex = i;
					seTaken = true;
					sep = close;
					spec = 0;
				}
			}
		}


		// If we are short and just snapped out of a bearish cloud, take a partial exit
		if (currentlyShortSeries[i] && !bearishCloudSeries[i] && bearishCloudSeries[i - 1] && i - lastShortEntryIndex > 0.618 * FastMAPeriod) {
			speTaken = true;
			spec++;
			// If we have reached the max partial exits, mark it as a final partial exit
			if (spec == ShortPartialExits) {
				sfeTaken = true;
				// And mark the short position as closed
				currentlyShortSeries[i] = false;
			}
			float realized = sep - close;
			if (realized >= floatZero) winningShortTrades++;
			totalShortTrades++;
		}



		// If we are in a bullish cloud
		if (bullishCloudSeries[i]) {
			// And we are short with partial exits remaining
			if (currentlyShortSeries[i]) {
				if (canTakeShorts && spec < ShortPartialExits) {
					// Take a final short partial (not reversal) exit.
					speTaken = true;
					sfeTaken = true;
					sreTaken = true;
					float realized = sep - close;
					if (realized >= floatZero) winningShortTrades++;
					spec++;
					totalShortTrades++;
				}
				currentlyShortSeries[i] = false;
			}

			if (canTakeLongs) {

				// Go long if not already
				if (!currentlyLongSeries[i]) {
					currentlyLongSeries[i] = true;
					lastLongEntryIndex = i;
					leTaken = true;
					lep = close;
					lpec = 0;
				}
			}
		}

		// If we are long and just snapped out of a bullish cloud, take a partial exit
		if (currentlyLongSeries[i] && !bullishCloudSeries[i] && bullishCloudSeries[i - 1] && i - lastLongEntryIndex > 0.618 * FastMAPeriod) {
			lpeTaken = true;
			lpec++;
			// If we have reached the max partial exits, mark it as a final partial exit
			if (lpec == LongPartialExits) {
				lfeTaken = true;
				// And mark the short position as closed
				currentlyLongSeries[i] = false;
			}
			float realized = close - lep;
			if (realized >= floatZero) winningLongTrades++;
			totalLongTrades++;
		}

		signal = (leTaken && !sreTaken) ? 0.5 : (leTaken && sreTaken) ? 10.0 :
			(seTaken && !lreTaken) ? 1.0 : (seTaken && lreTaken) ? 13.0 :
			(lpeTaken && !lfeTaken) ? 2.0 : (lpeTaken && lfeTaken) ? 4.0 :
			(speTaken && !sfeTaken) ? 3.0 : (speTaken && sfeTaken) ? 5.0 :
			0.0;

		float winningLongTrades_Float = winningLongTrades;
		float totalLongTrades_Float = totalLongTrades;
		float longEfficiency = (winningLongTrades_Float / (max(1, totalLongTrades_Float))) * 100.0F;

		float winningShortTrades_Float = winningShortTrades;
		float totalShortTrades_Float = totalShortTrades;
		float shortEfficiency = (winningShortTrades_Float / (max(1, totalShortTrades_Float))) * 100.0F;

		a_psResult->psResultArray->pfValue[i] = shortEfficiency;
	}






	// Basic error handling
	if (a_psResult->psResultArray->iFirstValid < 0 || a_psResult->psResultArray->iLastValid < 0
		|| a_psResult->psResultArray->iLastValid < a_psResult->psResultArray->iFirstValid ||
		a_psResult->psResultArray->iFirstValid < dataRec->sClose.iFirstValid ||
		a_psResult->psResultArray->iLastValid > dataRec->sClose.iLastValid)
		returnValue = MSX_ERROR;

	delete[] atrSeries;
	delete[] bearishCloudSeries;
	delete[] bullishCloudSeries;
	delete[] currentlyLongSeries;
	delete[] currentlyShortSeries;


	return returnValue;
}
