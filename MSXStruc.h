#ifndef MSX_Structures_h
#define MSX_Structures_h
/*
  Structures required for MetaStock External Function DLL interface
*/
#ifndef BOOL
typedef int BOOL;
#endif
#ifndef TRUE
#define TRUE 1
#endif
#ifndef FALSE
#define FALSE 0
#endif
// --------------------------------------------------------------------------
// Return this DLL version constant
// --------------------------------------------------------------------------
const int MSX_VERSION = 1;
// --------------------------------------------------------------------------
// Maximum number of aguments
// --------------------------------------------------------------------------
const int MSX_MAXARGS = 9;
// --------------------------------------------------------------------------
// Maximum string size (does not include MSXString arguments passed in to
//   external functions).
// --------------------------------------------------------------------------
const int MSX_MAXSTRING = 100;
// --------------------------------------------------------------------------
// The following two BOOL return values are returned from MSX functions
// --------------------------------------------------------------------------
const BOOL MSX_SUCCESS = FALSE;
const BOOL MSX_ERROR = TRUE;
// ----------------------------------------------------------------------------------------
// There are four potential argument types
// ----------------------------------------------------------------------------------------
const int MSXDataArray = 0;
const int MSXNumeric = 1;
const int MSXString = 2;
const int MSXCustom = 3;
// ----------------------------------------------------------------------------------------
// The following structure is used by the exported function MSXInfo
// ----------------------------------------------------------------------------------------
typedef struct
{
	char szCopyright[MSX_MAXSTRING];
	int  iNFuncs;
	int  iVersion;
} MSXDLLDef;
// ----------------------------------------------------------------------------------------
// The following structure is used by the exported function MSXNthFunction
// ----------------------------------------------------------------------------------------
typedef struct
{
	char  szFunctionName[MSX_MAXSTRING];
	char  szFunctionDescription[MSX_MAXSTRING];
	int   iNArguments;
} MSXFuncDef;
// ----------------------------------------------------------------------------------------
// The following structure is used by the exported function MSXNthArg
// ----------------------------------------------------------------------------------------
typedef struct
{
	int   iArgType;
	//  argtype constants:
	//   0 DataArray
	//   1 Numeric
	//   2 String
	//   3 CustomType
	char  szArgName[MSX_MAXSTRING];
	int   iNCustomStrings;
} MSXFuncArgDef;
// ----------------------------------------------------------------------------------------
// The following structure is used by the exported function MSXNthCustomString
// ----------------------------------------------------------------------------------------
typedef struct
{
	char  szString[MSX_MAXSTRING];
	int   iID;
} MSXFuncCustomString;
// ----------------------------------------------------------------------------------------
// the following datastructures are passed into and out of the user-written external
// calculation functions.
// ----------------------------------------------------------------------------------------
typedef struct
{
	long lDate;
	long lTime;
} MSXDateTime;
typedef struct
{
	float* pfValue;
	int   iFirstValid;
	int   iLastValid;
} MSXDataInfoRec;
typedef struct
{
	MSXDateTime* psDate;
	MSXDataInfoRec  sOpen;
	MSXDataInfoRec  sHigh;
	MSXDataInfoRec  sLow;
	MSXDataInfoRec  sClose;
	MSXDataInfoRec  sVol;
	MSXDataInfoRec  sOI;
	MSXDataInfoRec  sInd;
	char* pszSecurityName; // Security Name
	char* pszSymbol;       // Security Symbol
	char* pszSecurityPath; // Path where security is stored (may be in UNC format)
	char* pszOnlineSource; // Unused - reserved for future use...
	int             iPeriod;          // 'D'aily, 'W'eekly, 'M'onthly, 'Q'uarterly, 'I'ntraday
	int             iInterval;        // For period='I'ntraday only. 0=tick, other value = minutes compression.
	int             iStartTime;       // HHMM format. Undefined for non-intraday period.
	int             iEndTime;         // HHMM format. Undefined for non-intraday period.
	int             iSymbolType;      // Unused - reserved for future use
} MSXDataRec;
typedef struct
{				                                // possible for MSX_MAXARGS data arrays 
	MSXDataInfoRec* psDataInfoRecs[MSX_MAXARGS];  // pointers to the data arrays
	int            iNRecs;                        // number of arrays present (just a sanity check)
} MSXDataInfoRecArgsArray;
typedef struct
{
	float fNumerics[MSX_MAXARGS]; // possible for MSX_MAXARGS numerics
	int   iNRecs;                 // also a sanity check - func knows how many there should be.
} MSXNumericArgsArray;
typedef struct
{
	char* pszStrings[MSX_MAXARGS]; // possible for MSX_MAXARGS strings
	int  iNRecs;                   // ditto the above
} MSXStringArgsArray;
typedef struct
{
	int   iCustomIDs[MSX_MAXARGS]; // numeric ID associated with a custom arg
	int   iNRecs;                  // ditto the above
} MSXCustomArgsArray;
typedef struct
{
	MSXDataInfoRec* psResultArray;     // Pointer to result array
	char           szExtendedError[MSX_MAXSTRING];  // Extended Error string
} MSXResultRec;
#endif
